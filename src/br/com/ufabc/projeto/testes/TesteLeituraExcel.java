package br.com.ufabc.projeto.testes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

// https://www.devmedia.com.br/apache-poi-manipulando-documentos-em-java/31778

public class TesteLeituraExcel {

	// private static void main String fileName =
	// "C:\\Users\\40413824\\Documents\\PlanilhaTeste\\planilhaTeste";

	public static void main(String[] args) throws IOException {

		FileInputStream arquivo = null;

		try {

			File file = new File("C:\\Users\\40413824\\Documents\\PlanilhaTeste\\planilhaTeste.xlsx");

			arquivo = new FileInputStream(file);

			// cria workbook = planilha com todas as abas
			XSSFWorkbook workbook = new XSSFWorkbook(arquivo);

			// acessa a primeira aba da planilha
			XSSFSheet sheet = workbook.getSheetAt(0);

			// retorna todas as linhas a planilha da aba 1
			Iterator<Row> rowIterator = sheet.iterator();

			// varrimento das linhas
			while (rowIterator.hasNext()) { // enquanto tiver valor na linha

				// recebe cada linha da planilha
				Row row = rowIterator.next();

				// pega todas as c�lulas da linha
				Iterator<Cell> cellIterator = row.cellIterator();

				// varre todas as c�lulas da linha atual
				while (cellIterator.hasNext()) {

					Cell cell = cellIterator.next();
					
					System.out.println(cell.getStringCellValue());

//					switch (cell.getCellType()) {
//
//					case Cell.CELL_TYPE_STRING:
//						System.out.println(cell.getStringCellValue());
//						;
//						break;
//					case Cell.CELL_TYPE_NUMERIC:
//						System.out.println(cell.getNumericCellValue());
//						break;
//					}
				}
			}
			arquivo.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println("Arquivo Excel n�o encontrado!");
		}

	}

}
