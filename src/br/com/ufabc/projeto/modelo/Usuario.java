package br.com.ufabc.projeto.modelo;

public abstract class Usuario {
	
	protected String nome;
	protected String cpf;
	protected int idade;
	protected int senha;
	
	//Adm: senha par
	//Agente de transito: senha impar
	public abstract void verificaSenha(int senha);
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public int getSenha() {
		return senha;
	}
	public void setSenha(int senha) {
		this.senha = senha;
	}
	

}
