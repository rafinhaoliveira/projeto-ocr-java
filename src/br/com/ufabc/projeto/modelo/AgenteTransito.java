package br.com.ufabc.projeto.modelo;

public class AgenteTransito extends Usuario {
	
	private int registroAgente;
	private String filial;
	
	public void criaAcessoAgente(String nome, int idade, String cpf, int senha, int registroAgente, String filial) {
		this.nome = super.nome;
		this.idade = super.idade;
		this.cpf = super.cpf;
		this.senha = super.senha;
		this.registroAgente = registroAgente;
		this.filial = filial;
		System.out.println("Nome: " + nome);
		System.out.println("Idade: " + idade);
		System.out.println("cpf: " + cpf);
		System.out.println("senha: " + senha);
		System.out.println("registro agente: " + registroAgente);
		System.out.println("filial: " + filial);
	}
	
	public void verificaSenha(int senha) {
		super.senha = senha;
		if(senha % 2 != 0) {
			System.out.println("Senha correta");
		}

		else {
			System.out.println("Senha incorreta");
		}
	}
	
	public int getRegistroAgente() {
		return registroAgente;
	}

	public void setRegistroAgente(int registroAgente) {
		this.registroAgente = registroAgente;
	}

	public String getFilial() {
		return filial;
	}

	public void setFilial(String filial) {
		this.filial = filial;
	}

}
