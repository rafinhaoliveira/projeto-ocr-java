package br.com.ufabc.projeto.modelo;

public class Administrador extends Usuario {
	
	private int codigoAdm;
	Administrador adm;
	
	public void criaAcessoAdm(String nome, int idade, String cpf, int senha, int codigoAdm) {
		this.nome = super.nome;
		this.idade = super.idade;
		this.cpf = super.cpf;
		this.senha = super.senha;
		this.codigoAdm = codigoAdm;
		System.out.println("Nome: " + nome);
		System.out.println("Idade: " + idade);
		System.out.println("cpf: " + cpf);
		System.out.println("senha: " + senha);
		System.out.println("codigoAdm: " + codigoAdm);
	}

	public void verificaSenha(int senha) {
		super.senha = senha;
		if(senha % 2 == 0) {
			System.out.println("Senha correta");
		}

		else {
			System.out.println("Senha incorreta");
		}
	}
	public int getCodigoAdm() {
		return codigoAdm;
	}

	public void setCodigoAdm(int codigoAdm) {
		this.codigoAdm = codigoAdm;
	}
	
	
}
